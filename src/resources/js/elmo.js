function initRunAllFailedJobs() {
    $('.run-failed').submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var overlayer = form.parents('div.job-name').find('div.overlayer');
        var env = form.find('select option:selected').val();

        overlayer.addClass('active');

        $.ajax({
                   url: 'view/runAllFailedJobsView.php',
                   type: 'post',
                   data: {
                       env: env
                   }
               });

    });
}

function initBuildParticularJob() {
    $('.build').bind('click', function (e) {
        e.preventDefault();
        var buildBtn = $(this);
        var env = $('.run-failed select option:selected').val();

        var jobName = $(this).parents('.job').find('.job-name a').text();

        $.ajax({
                   url: 'view/runParticularJobView.php',
                   type: 'post',
                   data: {
                       env: env,
                       jobName: jobName
                   },
                   success: function () {
                       buildBtn.removeClass('run-icon');
                       buildBtn.removeClass('build');
                       buildBtn.addClass('loader');
                       buildBtn.unbind('click');
                       buildBtn.bind('click', function (e) {
                           e.preventDefault();
                       })
                   }
               });
    });
}

function initRunRegressionJobs() {
    var alertMsg = 'Do you really want to run all regression tests? There are >250 jobs!';

    $('.run-all').submit(function (e) {
        e.preventDefault();
        var env = $('select option:selected').val();
        var form = $(this);
        var overlayer = form.parents('div.job-name').find('div.overlayer');

        if (confirm(alertMsg)) {
            overlayer.addClass('active');

            $.ajax({
                       url: 'view/runRegressionJobsView.php',
                       type: 'post',
                       data: {
                           env: env
                       }
                   });
        }
    });
}

jQuery(document).ready(function () {
    initRunAllFailedJobs();
    initBuildParticularJob();
    initRunRegressionJobs();
});




































