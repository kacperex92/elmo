<?php
require_once '../../vendor/autoload.php';
require_once '../app/RunJob.php';

$runJob = new RunJob();
$url = 'http://elmo.stepstone.com/view/Reg/view/Hydrogen/';

$env = isset($_POST['env']) ? $_POST['env'] : '';

echo $runJob->runRegressionJobs($url, $env);