<?php

use JenkinsKhan\Jenkins;

require_once 'Elmo.php';

class RunJob
{

    private $url;
    private $jenkins;

    /**
     * RunJob constructor.
     */
    public function __construct()
    {
        $this->url = 'http://elmo.stepstone.com';
        $this->jenkins = new Jenkins($this->url);
    }

    public function runAllFailedJobs($url, $env)
    {
        $jenkins = new Jenkins($url);

        $elmo = new Elmo();
        $allFailedJobs = $elmo->getAllFailedElmoJobs();

        try {
            for ($i = 0; $i < sizeof($allFailedJobs); $i++) {
                $jenkins->launchJob(rawurlencode($allFailedJobs[$i]['name']),
                    ['name' => 'SuiteType', 'env' => $env]);
            }
        } catch (InvalidArgumentException $exception) {
            return [
                'success' => false,
                'amountOfRunFailedJobs' => 0
            ];
        }

        return json_encode(
            [
                'success' => true,
                'amountOfRunFailedJobs' => sizeof($allFailedJobs)
            ]);
    }

    public function runParticularJob($url, $env, $jobName)
    {
        $jenkins = new Jenkins($url);

        try {
            $jenkins->launchJob(rawurlencode($jobName), ['name' => 'SuiteType', 'env' => $env]);

        } catch (InvalidArgumentException $exception) {
            print_r($exception);
        }

        return json_encode(
            [
                'success' => true,
                'jobName' => $jobName
            ]);
    }

    public function runRegressionJobs($url, $env)
    {
        $jenkins = new Jenkins($url);

        try {
            $jenkins->launchJob(rawurlencode('0.RUN ALL'), ['name' => 'SuiteType', 'env' => $env]);
        } catch (InvalidArgumentException $exception) {
            return json_encode([
                'success' => false
            ]);
        }

        return json_encode([
            'success' => true
        ]);
    }


}