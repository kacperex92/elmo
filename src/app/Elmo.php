<?php

class Elmo
{

    /**
     * Elmo constructor.
     */
    public function __construct()
    {

    }

    public function getAllJobs()
    {
        $content = file_get_contents("http://elmo.stepstone.com/view/Reg/view/Hydrogen/api/json");
        $result = json_decode($content, true);

        $jobs = [];
        for ($i = 0; $i < sizeof($result['jobs']); $i++) {
            $jobs[] = $result['jobs'][$i];
        }

        return $jobs;
    }

    public function getAllFailedElmoJobs()
    {
        $jobs = $this->getAllJobs();

        $failedJobs = [];
        for ($i = 0; $i < sizeof($jobs); $i++) {
            if ($jobs[$i]['color'] == 'yellow'
                || $jobs[$i]['color'] == 'red'
                || $jobs[$i]['color'] == 'aborted'
            ) {
                $failedJobs[] = $jobs[$i];
            }
        }

        return $failedJobs;
    }

    public function getRegressionJobsStarted($regressionJobs)
    {
        $regressionJobsStarted = [];
        for ($i = 0; $i < sizeof($regressionJobs); $i++) {
            if (stristr($regressionJobs[$i]['color'], '_anime')) {
                $regressionJobsStarted[] = $regressionJobs[$i];
            }
        }
        return $regressionJobsStarted;
    }

    public function getFailedJobsPerHeadline()
    {
        $failedJobs = $this->getAllFailedElmoJobs();

        $headlines = [
            'SEPP',
            'cv_',
            'CV ZA MOBILE',
            'Auth Hash',
            'Email acquisition',
            'Remember me',
            'Register page',
            'Job Advisor',
            'Login page'
        ];

        $failedJobsPerHeadline = [];
        for ($i = 0; $i < sizeof($failedJobs); $i++) {
            foreach ($headlines as $headline) {
                if (stristr($failedJobs[$i]['name'], $headline)) {
                    $failedJobsPerHeadline[$headline][] = $failedJobs[$i];
                }
            }
        }

        return $failedJobsPerHeadline;
    }

    public function getAllRegressionJobs()
    {
        $jobs = $this->getAllJobs();

        $regressionJobNames = [
            'SEPP',
            'CV_',
            'AT Auth Hash',
            'Job Advisor',
            'RWD Register PAGE',
            'RWD Login PAGE',
            'CV ZA MOBILE'
        ];

        $regressionJobNamesExcluded = [
            '0.SEPP ALL',
            '0.SEPP AT ALL',
            '0.SEPP BE ALL',
            '0.SEPP DE ALL',
            '0.SEPP FR ALL',
            '0.SEPP NL ALL',
            '0.CV_AT ALL',
            '0.CV_BE ALL',
            '0.CV_DE ALL',
            '0.CV_FR ALL',
            '0.CV_NL ALL',
            '0.CV_ZA ALL',
            '0. Auth Hash ALL',
            '0.Job Advisor ALL',
            '0.RWD Register page',
            '0.RWD Login page',
            '0. CV ZA MOBILE ALL',
        ];

        $regressionJobs = [];
        for ($i = 0; $i < sizeof($jobs); $i++) {

            if (stristr($jobs[$i]['name'], "SEPP") && !stristr($jobs[$i]['name'], "0.SEPP")) {
                $regressionJobs[] = $jobs[$i];
            }

            if (stristr($jobs[$i]['name'], "CV_") && !stristr($jobs[$i]['name'], '0.CV_')) {
                $regressionJobs[] = $jobs[$i];
            }

            if (stristr($jobs[$i]['name'], "AT Auth Hash")
                && !stristr($jobs[$i]['name'], '0. Auth Hash ALL')
            ) {
                $regressionJobs[] = $jobs[$i];
            }

            if (stristr($jobs[$i]['name'], "Job Advisor")
                && !stristr($jobs[$i]['name'], '0.Job Advisor ALL')
            ) {
                $regressionJobs[] = $jobs[$i];
            }

            if (stristr($jobs[$i]['name'], "RWD Register PAGE")
                && !stristr($jobs[$i]['name'], "0.RWD Register")
            ) {
                $regressionJobs[] = $jobs[$i];
            }

            if (stristr($jobs[$i]['name'], "RWD Login PAGE")
                && !stristr($jobs[$i]['name'], "0.RWD Login")
            ) {
                $regressionJobs[] = $jobs[$i];
            }

            if (stristr($jobs[$i]['name'], "CV ZA MOBILE")
                && !stristr($jobs[$i]['name'], "0. CV ZA MOBILE ALL")
            ) {
                $regressionJobs[] = $jobs[$i];
            }
        }
        return $regressionJobs;
    }

    public function getAllFailedRegressionJobs($regressionJobs)
    {
        $failedRegressionJobs = [];
        for ($i = 0; $i < sizeof($regressionJobs); $i++) {
            if ($regressionJobs[$i]['color'] == 'yellow' || $regressionJobs[$i]['color'] == 'red'
                || $regressionJobs[$i]['color'] == 'aborted'
            ) {
                $failedRegressionJobs[] = $regressionJobs[$i];
            }
        }
        return $failedRegressionJobs;
    }

    public function getQueue()
    {
        $url = 'http://elmo.stepstone.com/queue/api/json?pretty=true';
        $content = file_get_contents($url);
        $result = json_decode($content, true);
        return $result['items'];
    }

    public function getPercentOfFailedJobs($failedJobs = [], $allJobs = [])
    {
        return ceil(sizeof($failedJobs)/sizeof($allJobs)*100);
    }

}





