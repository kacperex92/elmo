<?php
if (http_response_code() != 200) {
    echo 'Problem with Jenkins stability!<br/>';
    echo 'Error cause: ' . http_response_code();
} else {

    require_once '../vendor/autoload.php';
    require_once 'app/Elmo.php';

    $url = 'http://elmo.stepstone.com';

    $elmo = new Elmo();

    $allJobs = $elmo->getAllJobs();
    $jobsInProgress = $elmo->getRegressionJobsStarted($allJobs);
    $jobsFailed = $elmo->getAllFailedRegressionJobs($allJobs);
    $percentOfFailedJobs = $elmo->getPercentOfFailedJobs($jobsFailed, $allJobs);
    $queue = sizeof($elmo->getQueue());

    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Title</title>
        <link rel="stylesheet" href="resources/css/style.css"/>
        <script src="resources/js/jquery.js"></script>
        <script src="resources/js/elmo.js"></script>
    </head>
    <body>

    <div class="container">
        <div class="headline">REGRESSION
            <?php
            echo '(all - ';
            echo sizeof($allJobs);
            echo ' | in progress - ';
            echo sizeof($jobsInProgress);
            echo ' | failed - ';
            echo sizeof($jobsFailed);
            echo '| queue - ';
            echo $queue;
            echo ')';
            echo '<br/>';
            echo '<span class="failed-jobs-percent">'.$percentOfFailedJobs.'% of failed tests</span>';
            ?>

        </div>
        <div class="jobsPerGroup">
            <div class="job clearfix">
                <div class="job-name">
                    <div class="overlayer"></div>
                    <div class="job-name-item run-all-item-name">Run all</div>
                    <form class="run-all form-default" method="get" action="">
                        <select class="form-control" required>
                            <option>www</option>
                            <option>next</option>
                            <option>latest</option>
                            <option>build</option>
                            <option>hydrogen-team</option>
                        </select>
                        <button type="submit" class="btn btn-submit">Buduj</button>
                    </form>
                </div>
            </div>
            <div class="job clearfix">
                <div class="job-name">
                    <div class="overlayer"></div>
                    <div class="job-name-item">Run failed</div>
                    <form class="run-failed form-default" method="get" action="">
                        <select class="form-control" required>
                            <option>www</option>
                            <option>next</option>
                            <option>latest</option>
                            <option>build</option>
                            <option>hydrogen-team</option>
                        </select>
                        <button type="submit" class="btn btn-submit">Buduj</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <?php
        $failedJobsOrderByHeadline = $elmo->getFailedJobsPerHeadline();

        foreach ($failedJobsOrderByHeadline as $headline => $value) {
            echo "<div class=headline>$headline (" . (sizeof($failedJobsOrderByHeadline[$headline]))
                . ")</div>";
            echo '<div class="jobsPerGroup">';
            for ($i = 0; $i < sizeof($value); $i++) {
                $url = $value[$i]['url'] . 'build';
                $runJob = '<a class="build run-icon" href="" title="Run job"></a>';
                $jobName = $value[$i]['name'];

                echo '<div class="job clearfix">';
                echo '<div class="overlayer"></div>';
                echo '<div class="job-name">';
                echo '<a href="' . $url . '">' . $jobName . '</a>';
                echo '</div>';

                echo '<div class="job-build">';
                echo $runJob;
                echo '</div>';
                echo '</div>';
            }

            echo '</div>';
        }
        ?>
    </div>

    </body>
    </html>
    <?php
}
?>