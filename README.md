# README #

Elmo regression tests

### What is this repository for? ###

* This project helps run regression tests from elmo by clicking on chosen tests
* 0.0.1
* [Get repository](https://bitbucket.org/kacperex92/elmo)

### How do I get set up? ###

* git clone https://bitbucket.org/kacperex92/elmo

![Screenshot](https://bytebucket.org/kacperex92/elmo/raw/6c6231b425a555bb4a35c7d46c4dec26db395005/src/resources/img/example.png)

### Contribution guidelines ###


### Do you want to find out more? ###

* Repo admin: [kacperex92](mailto:kacperex92@gmail.com)